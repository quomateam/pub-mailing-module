<?php

namespace quoma\modules\mailing;

use quoma\core\module\QuomaModule;
use quoma\core\menu\Menu;

class MailingModule extends QuomaModule
{
    public $controllerNamespace = 'quoma\modules\mailing\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @return Menu
     */
    public function getMenu(Menu $menu)
    {
        return null;
    }

    public function getDependencies()
    {
        return [
        ];
    }
}
