<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 24/07/15
 * Time: 23:23
 */

namespace quoma\modules\mailing\components\sender;


use Yii;

class MailSender
{
    /**
     * @var
     */
    private $mailer;

    /**
     * Nombre de la configuracion de transporte, configurada en params
     * @var string
     */
    private $config = array();

    public function __construct($config)
    {
        $this->config = Yii::$app->params["mailing"][$config];
        $this->mailer = Yii::$app->getMailer();

        if (isset($this->config['transport'])){
            $this->mailer->setTransport($this->config['transport']);
        }

        // Si en la configuracion tengo un template lo pongo.
        if(trim($this->config['layout'])!="") {
            $this->mailer->htmlLayout = $this->config['layout'];
        }
    }

    public function send($to, $subject = "", $content = [],
                         $cc = [], $bcc = [], $attachment = [])
    {
        try {
            $message = $this->mailer->compose($content['view'], $content['params']);

            $message->setFrom($this->config['from'])
                ->setTo($to)
                ->setSubject($subject);

            if (is_array($cc)!="") {
                $message->setCc($cc);
            }

            if (is_array($bcc)!="") {
                $message->setBcc($bcc);
            }
            // Los attachmentes, pueden ser con un archivo del file system o con contenido on-defly
            if(!empty($attachment)) {
                foreach($attachment as $attach) {
                    if(is_array($attach)) {
                        $message->attachContent($attach['view'], $attach['options']);
                    } else {
                        if (file_exists($attach)) {
                            $message->attach($attach);
                        }
                    }
                }

            }
            if(!$message->send()) {
                throw new \Exception('The email cant be sended.');
            }
        }catch(\Exception $ex) {
            throw $ex;
        }

    }
}